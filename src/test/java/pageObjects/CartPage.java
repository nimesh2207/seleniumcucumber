package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.CommonMethods;

import java.util.List;

public class CartPage extends CommonMethods {

    private WebDriver driver;

    @FindBy(xpath = "//button[@id='checkout']")
    WebElement btnCheckout;

    @FindBy(css = "div.inventory_item_name")
    List<WebElement> cartItemList;

    @FindBy(css = "span.shopping_cart_badge")
    WebElement labelItemCount;

    public CartPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(this.driver,this);
    }

    public void removeItemFromCartPage(String itemName)
    {
        for (WebElement item : cartItemList) {
            if (getText(item).equals(itemName)) {
                WebElement btnRemove = item.findElement(By.xpath(".//ancestor::div[@class='cart_item']//button[text()='Remove']"));
                doClick(btnRemove);
                break;
            }
        }
    }

    public void clickCheckout()
    {
        doClick(btnCheckout);
    }

    public int getCartItemCountFromCartPage()
    {
        return Integer.valueOf(getText(labelItemCount));
    }
}
