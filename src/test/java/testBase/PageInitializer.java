package testBase;

import pageObjects.*;

public class PageInitializer extends BaseClass {
	
	public static LoginPage loginPage;
	public static HomePage homePage;
	public static ProductDetailPage productPage;
	public static CartPage cartPage;
	public static Checkout_UserInfoPage checkout_userInfoPage;
	public static Checkout_Overview checkout_overview;
	public static OrderSummaryPage orderSummaryPage;
	
	public static void initialize()
	{
		loginPage = new LoginPage(driver);
		homePage = new HomePage(driver);
		productPage = new ProductDetailPage(driver);
		cartPage = new CartPage(driver);
		checkout_userInfoPage = new Checkout_UserInfoPage(driver);
		checkout_overview = new Checkout_Overview(driver);
		orderSummaryPage = new OrderSummaryPage(driver);
	}

}
