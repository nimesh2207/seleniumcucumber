package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utils.CommonMethods;

public class LoginPage extends CommonMethods{
	
	private WebDriver driver;
	
	@FindBy(css = "input#user-name")
	public WebElement txtUserName;
	
	@FindBy(css = "input#password")
	public WebElement txtPassword;
	
	@FindBy(css = "input#login-button")
	public WebElement btnLogin;
	
	@FindBy(css = "h3")
	public WebElement labelErrorMessage;

	@FindBy(css = "button#react-burger-menu-btn")
	public WebElement btnMenu;

	@FindBy(css = "a#logout_sidebar_link")
	public WebElement btnLogout;

	@FindBy(css = "div.login_logo")
	public WebElement logoLoginPage;

	public LoginPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void setUserName(String userName)
	{
		setText(txtUserName, userName);
	}
	
	public void setPassword(String password)
	{
		setText(txtPassword, password);
	}
	
	public void clickLogin()
	{
		doClick(btnLogin);
	}
	
	public void login(String userName, String password)
	{
		setText(txtUserName, userName);
		setText(txtPassword, password);
		doClick(btnLogin);
	}

	public void logout()
	{
		doClick(btnMenu);
		doClick(btnLogout);
	}
}
