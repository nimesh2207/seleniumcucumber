package steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import utils.CommonMethods;

public class LoginSteps extends CommonMethods {

    @When("user enters valid username as {string}")
    public void user_enters_valid_username_as(String userName) {
        loginPage.setUserName(userName);
    }

    @When("user enter valid password as {string}")
    public void user_enter_valid_password_as(String password) {
        loginPage.setPassword(password);
    }
    @When("user click login button")
    public void user_click_login_button() {
        loginPage.clickLogin();
    }
    @Then("user should be allowed to login successfully")
    public void user_should_be_allowed_to_login_successfully() {
        String labelHomePage = getText(homePage.labelHomePage);
        Assert.assertEquals("Home Page label verification","PRODUCTS",labelHomePage);
    }
    @When("user logout account")
    public void user_logout_account() {
        loginPage.logout();
    }
    @Then("user should be allowed to logout successfully")
    public void user_should_be_allowed_to_logout_successfully() {
        boolean isLogoDisplayed = loginPage.logoLoginPage.isDisplayed();
        Assert.assertTrue("Login page Logo verification",isLogoDisplayed);
    }


    @Then("application should display {string}")
    public void application_should_display(String message) {
        String actErrorMsg = getText(loginPage.labelErrorMessage);
        Assert.assertTrue("Error message verification",actErrorMsg.contains(message));
    }
}
