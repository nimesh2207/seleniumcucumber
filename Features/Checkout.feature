@checkout
Feature: Test checkout feature

  Background:
    Given user is actually successfully logged in with this credentials
      | username      | password     |
      | standard_user | secret_sauce |

  @singleProduct @smoke @regression
  Scenario Outline: Order Checkout
    When I click on item with name "<Item>"
    And I click on the ADD TO CART button
    Then count of products in cart from Cart Page should be <ItemCount>
    And I click on the shopping cart button
    And I click on the checkout button
    And I enter the order information as "<FirstName>", "<LastName>", and "<Zip>"
    And I click on continue button
    Then I verify the item name matches that of "<Item>"
    And I verify order confirmation on Order summary page
    When user logout account
    Then user should be allowed to logout successfully

    Examples:
      | Item                     | FirstName | LastName | Zip   | ItemCount |
      | Sauce Labs Bike Light    | test      | test     | 12345 | 1         |
      | Sauce Labs Fleece Jacket | test2     | test2    | 23456 | 1         |

  @multipleProducts @regression
  Scenario: Order Checkout
    When I click on below items to cart
      | Sauce Labs Backpack      |
      | Sauce Labs Bolt T-Shirt  |
      | Sauce Labs Fleece Jacket |
    Then count of products in cart from Home Page should be 3
    And I click on the shopping cart button
    And I click on the checkout button
    And I enter the order information as below
      | test2 | test2 | 12356 |
    And I click on continue button
    Then I verify the item name matches
      | Sauce Labs Backpack      |
      | Sauce Labs Bolt T-Shirt  |
      | Sauce Labs Fleece Jacket |
    And I verify order confirmation on Order summary page
    When user logout account
    Then user should be allowed to logout successfully

  @removeProductsHomePage @regression
  Scenario: Order Checkout
    When I click on below items to cart
      | Sauce Labs Backpack      |
      | Sauce Labs Bolt T-Shirt  |
      | Sauce Labs Fleece Jacket |
      | Sauce Labs Onesie        |
    Then count of products in cart from Home Page should be 4
    When I remove products from Home Page
      | Sauce Labs Fleece Jacket |
      | Sauce Labs Bolt T-Shirt  |
    Then count of products in cart from Home Page should be 2
    And I click on the shopping cart button
    And I click on the checkout button
    And I enter the order information as below
      | test2 | test2 | 12356 |
    And I click on continue button
    Then I verify the item name matches
      | Sauce Labs Backpack      |
      | Sauce Labs Onesie        |
    And I verify order confirmation on Order summary page
    When user logout account
    Then user should be allowed to logout successfully

  @removeProductsCartPage @regression
  Scenario: Order Checkout
    When I click on below items to cart
      | Sauce Labs Backpack      |
      | Sauce Labs Bolt T-Shirt  |
      | Sauce Labs Fleece Jacket |
      | Sauce Labs Onesie        |
    And I click on the shopping cart button
    Then count of products in cart from Cart Page should be 4
    When I remove products from Cart Page
      | Sauce Labs Fleece Jacket |
      | Sauce Labs Bolt T-Shirt  |
    Then count of products in cart from Cart Page should be 2
    And I click on the checkout button
    And I enter the order information as below
      | test2 | test2 | 12356 |
    And I click on continue button
    Then I verify the item name matches
      | Sauce Labs Backpack      |
      | Sauce Labs Onesie        |
    And I verify order confirmation on Order summary page
    When user logout account
    Then user should be allowed to logout successfully

