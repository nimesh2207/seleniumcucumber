package steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import utils.CommonMethods;

import java.util.List;
import java.util.Map;

public class CheckoutSteps extends CommonMethods {

    @Given("user is actually successfully logged in with this credentials")
    public void user_is_actually_successfully_logged_in_with_this_credentials(DataTable loginDetails) {
        List<Map<String,String>> loginCredentials = loginDetails.asMaps();

        loginPage.login(loginCredentials.get(0).get("username"),
                loginCredentials.get(0).get("password"));
    }

    @When("I click on item with name {string}")
    public void i_click_on_item_with_name(String item) {
        homePage.selectItem(item);
    }
    @When("I click on the ADD TO CART button")
    public void i_click_on_the_add_to_cart_button() {
        productPage.clickAddToCart();
    }
    @When("I click on the shopping cart button")
    public void i_click_on_the_shopping_cart_button() {
        productPage.openCart();
    }
    @When("I click on the checkout button")
    public void i_click_on_the_checkout_button() {
        cartPage.clickCheckout();
    }
    @When("I enter the order information as {string}, {string}, and {string}")
    public void i_enter_the_order_information_as_and(String fName, String lName, String zip) {
        checkout_userInfoPage.setUserInformation(fName,lName,zip);
    }
    @When("I click on continue button")
    public void i_click_on_continue_button() {
        checkout_userInfoPage.clickContinueButton();
    }
    @Then("I verify the item name matches that of {string}")
    public void i_verify_the_item_name_matches_that_of(String item) {

        Assert.assertTrue("Verification of item in Checkout page",checkout_overview.verifyProductOnCheckoutPage(item));
        checkout_overview.clickFinish();
    }

    @Then("I verify order confirmation on Order summary page")
    public void i_verify_order_confirmation_on_order_summary_page() {
        String confirmationMsg = orderSummaryPage.getOrderConfirmationMessage();
        Assert.assertEquals("Order confirmation Message",
                "Your order has been dispatched, and will arrive just as fast as the pony can get there!",
                confirmationMsg);
    }


    @When("I click on below items to cart")
    public void i_click_on_below_items_to_cart(DataTable itemList) {
        List<String> items = itemList.asList();

        for(String item : items)
        {
            homePage.addItemToCartFromHomePage(item);
        }
    }

    @When("I enter the order information as below")
    public void i_enter_the_order_information_as_below(DataTable userInfo) {
        List<List<String>> userData = userInfo.asLists();
        checkout_userInfoPage.setUserInformation(userData.get(0).get(0),
                userData.get(0).get(1),
                userData.get(0).get(2));
    }
    @Then("I verify the item name matches")
    public void i_verify_the_item_name_matches(DataTable itemList) {

        List<String> items = itemList.asList();

        for(String item: items)
        {
            Assert.assertTrue("Verification of item in Checkout page",
                    checkout_overview.verifyProductOnCheckoutPage(item));
        }
        checkout_overview.clickFinish();
    }

    @Then("count of products in cart from Cart Page should be {int}")
    public void count_of_products_in_cart_from_cart_page_should_be(int expCount) {
        int actCount = cartPage.getCartItemCountFromCartPage();
        Assert.assertEquals("Cart item Count verification",expCount,actCount);
    }

    @Then("count of products in cart from Home Page should be {int}")
    public void count_of_products_in_cart_from_home_page_should_be(int expCount) {
        int actCount = homePage.getCartItemCountFromHomePage();
        Assert.assertEquals("Cart item Count verification",expCount,actCount);
    }

    @When("I remove products from Home Page")
    public void i_remove_products_from_home_page(DataTable itemList) {
        List<String> items = itemList.asList();

        for(String item : items)
        {
            homePage.removeItemFromCartFromHomePage(item);
        }
    }

    @When("I remove products from Cart Page")
    public void i_remove_products_from_cart_page(DataTable itemList) {
        List<String> items = itemList.asList();

        for(String item : items)
        {
            cartPage.removeItemFromCartPage(item);
        }
    }

}
