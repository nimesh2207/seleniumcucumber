package testRunner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "Features",
        glue = "steps",
        monochrome = true,
        dryRun = false,
        tags = "@regression",
        plugin = {"pretty",
                "html:logs/cucumber-report.html"
        }
)
public class MyTestRunner {
}
