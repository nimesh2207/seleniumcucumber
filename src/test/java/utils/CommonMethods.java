package utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;

import testBase.PageInitializer;

public class CommonMethods extends PageInitializer
{
	
	public static void setText(WebElement element,String value)
	{
		element.clear();
		element.sendKeys(value);
	}
	
	public static void doClick(WebElement element)
	{
		element.click();
	}
	
	public static String getText(WebElement element)
	{
		return element.getText();
	}
	
	public static byte[] takeScreenshot(String filename) {
		TakesScreenshot ts = (TakesScreenshot) driver;

		byte[] screenshot = ts.getScreenshotAs(OutputType.BYTES);

				File file = ts.getScreenshotAs(OutputType.FILE);
		// create destination as : filepath + filename + timestamp + .png
		String destination = Constants.SCREENSHOT_FILEPATH + filename + ".png";

		try {
			FileUtils.copyFile(file, new File(destination));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return screenshot;
	}
	
	public static void scrollToElement(WebElement element)
	{
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}
}

