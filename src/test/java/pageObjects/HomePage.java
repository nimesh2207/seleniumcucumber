package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.CommonMethods;

import java.util.List;

public class HomePage extends CommonMethods {

    private WebDriver driver;

    @FindBy(css = "div.header_secondary_container>span")
    public WebElement labelHomePage;

    @FindBy(css = "div.inventory_item_name")
    public List<WebElement> itemList;

    @FindBy(css = "span.shopping_cart_badge")
    WebElement labelItemCount;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    public void selectItem(String itemName) {
        for (WebElement item : itemList) {
            if (getText(item).equals(itemName)) {
                doClick(item);
                break;
            }
        }
    }

    public void addItemToCartFromHomePage(String itemName) {
        for (WebElement item : itemList) {
            if (getText(item).equals(itemName)) {
                WebElement btnAddToCart = item.findElement(By.xpath(".//ancestor::div[@class='inventory_item']//button[text()='Add to cart']"));
                doClick(btnAddToCart);
                break;
            }
        }
    }

    public void removeItemFromCartFromHomePage(String itemName) {
        for (WebElement item : itemList) {
            if (getText(item).equals(itemName)) {
                WebElement btnRemove = item.findElement(By.xpath(".//ancestor::div[@class='inventory_item']//button[text()='Remove']"));
                doClick(btnRemove);
                break;
            }
        }
    }


    public int getCartItemCountFromHomePage()
    {
        return Integer.valueOf(getText(labelItemCount));
    }

}
