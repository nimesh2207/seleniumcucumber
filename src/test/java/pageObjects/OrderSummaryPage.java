package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.CommonMethods;

public class OrderSummaryPage extends CommonMethods {

    private WebDriver driver;

    @FindBy(css = "div.complete-text")
    WebElement labelOrderConfirmationMessage;

    public OrderSummaryPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(this.driver,this);
    }

    public String getOrderConfirmationMessage()
    {
        return getText(labelOrderConfirmationMessage);
    }
}
