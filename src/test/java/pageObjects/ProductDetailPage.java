package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.CommonMethods;

public class ProductDetailPage extends CommonMethods {

    private WebDriver driver;

    @FindBy(xpath = "//button[text()='Add to cart']")
    WebElement btnAddToCart;

    @FindBy(css = "div#shopping_cart_container>a")
    WebElement btnCart;

    public ProductDetailPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(this.driver,this);
    }

    public void openCart()
    {
        doClick(btnCart);
    }

    public void clickAddToCart()
    {
        doClick(btnAddToCart);
    }
}
