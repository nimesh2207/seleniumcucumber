@login
Feature: Test Login Feature

  @valid @smoke @regression
  Scenario Outline: Login with Valid Credentials
    When user enters valid username as "<username>"
    And user enter valid password as "<password>"
    And user click login button
    Then user should be allowed to login successfully
    When user logout account
    Then user should be allowed to logout successfully
    Examples:
      | username                | password     |
      | standard_user           | secret_sauce |
      | problem_user            | secret_sauce |
      | performance_glitch_user | secret_sauce |

    @invalid @regression
  Scenario Outline: Login with invalid Combinations
    When user enters valid username as "<username>"
    And user enter valid password as "<password>"
    And user click login button
    Then application should display "<msg>"
    Examples:
      | username                | password     | msg                                                         |
      | standard_user           |              | Password is required                                        |
      | problem_user            |              | Password is required                                        |
      | performance_glitch_user |              | Password is required                                        |
      |                         | secret_sauce | Username is required                                        |
      | standard_user           | testPassword | Username and password do not match any user in this service |
      | dummyUser               | secret_sauce | Username and password do not match any user in this service |