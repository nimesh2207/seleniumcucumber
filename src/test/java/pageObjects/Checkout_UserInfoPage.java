package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.CommonMethods;

public class Checkout_UserInfoPage extends CommonMethods {

    private WebDriver driver;

    @FindBy(css = "input#first-name")
    WebElement txtFirstName;

    @FindBy(css = "input#last-name")
    WebElement txtLastName;

    @FindBy(css = "input#postal-code")
    WebElement txtPostalCode;

    @FindBy(xpath = "//input[@id='continue']")
    WebElement btnContinue;

    public Checkout_UserInfoPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(this.driver,this);
    }

    public void setUserInformation(String fName,String lName,String zip)
    {
        setText(txtFirstName,fName);
        setText(txtLastName,lName);
        setText(txtPostalCode,zip);
    }

    public void clickContinueButton()
    {
        doClick(btnContinue);
    }
}
