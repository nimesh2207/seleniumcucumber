package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.CommonMethods;

import java.util.List;

public class Checkout_Overview extends CommonMethods {

    private WebDriver driver;

    @FindBy(css = "div.inventory_item_name")
    List<WebElement> cartItemList;

    @FindBy(xpath = "//button[text()='Finish']")
    WebElement btnFinish;

    public Checkout_Overview(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(this.driver,this);
    }

    public boolean verifyProductOnCheckoutPage(String itemName)
    {
        boolean isProductDisplayed = false;

        for(WebElement item : cartItemList)
        {
            if(getText(item).equals(itemName)) {
                isProductDisplayed = true;
                break;
            }
        }

        return isProductDisplayed;
    }

    public void clickFinish()
    {
    	scrollToElement(btnFinish);
        doClick(btnFinish);
    }
}
